import React, { Component } from 'react';
import Webhooks from './components/webhooks';
import { Navbar, NavbarBrand } from 'reactstrap'

class App extends Component {

  state = {
    webhooks: []
  }
  getItems() {
    fetch(process.env.REACT_APP_API_URL)
    .then(res => res.json())
    .then((data) => {
      this.setState({ webhooks: data })
    })
    .catch(console.log)
  }

  componentDidMount() {
    this.getItems()
    this.timer = setInterval(()=> this.getItems(), 10000);
  }

  componentWillUnmount() {
    this.getItems()
    this.timer = null;
  }

  render() {
    return (
      <div>
        <div>
          <Navbar color="light" light expand="md">
            <NavbarBrand href="/">Terraform Webhook Monitor</NavbarBrand>
          </Navbar>
        </div>
        <div>
        <body>
          <Webhooks webhooks={this.state.webhooks} />
          </body>
        </div>
      </div>
    )
   }
}

 export default App;
