import React from 'react'
import PayloadDetail from './payload'
import Timestamp from 'react-timestamp'
import { Table } from 'reactstrap'

const Webhooks = ({ webhooks }) => {  
  return (
    <div>
      <Table>
        <thead>
          <tr>
            <th>Timestamp</th>
            <th>Organization</th>
            <th>Workspace</th>
            <th>Run Message</th>
            <th>Run Status</th>
            <th>Message</th>
            <th>Full Payload</th>
            <th>Date</th>
          
          </tr>
        </thead>
        <tbody>
            {webhooks.map((webhook) => (

                        <tr>
                          <td><Timestamp date={webhook.run_created_at} /></td>
                          <td>{webhook.organization_name}</td>
                          <td>{webhook.workspace_name}</td>
                          <td>{webhook.run_message}</td>
                          <td>{webhook.notifications[0].message}</td>
                          <td>{webhook.notifications[0].run_status}</td>
                          <td><PayloadDetail props = {webhook} /></td>
 
                        </tr>




            ))}
        </tbody>
      </Table>
    </div>
  )
};

export default Webhooks;
