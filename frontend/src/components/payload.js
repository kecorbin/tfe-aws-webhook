import React, { useState } from 'react';
import { Collapse, CardBody, Card } from 'reactstrap';
import ReactJson from 'react-json-view'

const PayloadDetail = (props) => {
  const [collapse, setCollapse] = useState(false);
  const [status, setStatus] = useState('Closed');

  const onEntering = () => setStatus('Opening...');

  const onEntered = () => setStatus('Opened');

  const onExiting = () => setStatus('Closing...');

  const onExited = () => setStatus('Closed');

  const toggle = () => setCollapse(!collapse);
  console.log(status)
  return (
    <div>
      <button><a href onClick={toggle} style={{ marginTop: '1rem' }}>View Payload</a></button>

      <Collapse
        isOpen={collapse}
        onEntering={onEntering}
        onEntered={onEntered}
        onExiting={onExiting}
        onExited={onExited}
      >
        <Card>
          <CardBody>
            <ReactJson src={props}/>

          </CardBody>
        </Card>
      </Collapse>
    </div>
  );
}

export default PayloadDetail;
