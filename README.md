# tfe-aws-webhook

A lambda function designed to monitor webhook notifications from Terraform Enterprise, deployable via Terraform

A react based web frontend that presents information collected from the lambda function, hosted in AWS S3


## Demo

This repo is also designed to demonstrate CI/CD with Terraform, Vault and Gitlab, to run the full environment you will need a vault environment which is reachable from your gitlab runners


## Vault Setup (prerequisite)

### Configure Gitlab Runner Policy

```
path "kv/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
  }

path "aws/creds/webhook-s3-creds" {
  capabilities = ["read"]
  }
```

### Enable App Role 

```
vault secrets enable approle
vault write auth/approle/role/gitlab_runner \
    policies="gitlab_runner" \
    token_ttl=20m \
    token_max_ttl=30m

vault read auth/approle/role/gitlab_runner/role-id

```
Note the role-id


### Create IAM policy for this role

```

vault write vault read aws/roles/webhook-s3-creds max_ttl=15m credential_type=iam_user policy_document=-<<EOF

{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "arn:aws:s3:::tfe-webhooks*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": [
        "arn:aws:s3:::tfe-webhooks*/*"
      ]
    }
  ]
}
EOF
```

### Test

```
vault write -f auth/approle/role/gitlab_runner/secret-id
```