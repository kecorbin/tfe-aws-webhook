FROM alpine
RUN apk update && apk add jq curl npm python3
RUN pip3 install awscli
RUN curl -s https://releases.hashicorp.com/terraform/0.12.23/terraform_0.12.23_linux_amd64.zip -o terraform.zip
RUN unzip terraform.zip && rm terraform.zip
RUN mv terraform /usr/local/bin/terraform && chmod +x /usr/local/bin/terraform
RUN curl -s https://releases.hashicorp.com/vault/1.3.3/vault_1.3.3_linux_amd64.zip -o vault.zip
RUN unzip vault.zip && rm vault.zip
RUN mv vault /usr/local/bin/vault && chmod +x /usr/local/bin/vault
RUN wget https://github.com/hashicorp-community/tf-helper/archive/0.2.8.zip -O /tmp/tfh.zip
RUN mkdir /opt/tfh
RUN unzip /tmp/tfh.zip -d /opt/ 
ENV PATH="/opt/tf-helper-0.2.8/tfh/bin:${PATH}"