
output "webhook_url" {
  value = "${aws_api_gateway_deployment.prod_api.invoke_url}/${aws_api_gateway_resource.proxy.path_part}"
}

output "website_endpoint" {
  value = "http://${aws_s3_bucket.react_bucket.website_endpoint}"
}
