data "archive_file" "webhook_receiver_zip" {
  type        = "zip"
  source_dir  = "../lambda_function/"
  output_path = "../lambda_function/webhook_receiver.zip"
}

resource "aws_lambda_function" "receiver_lambda" {
  filename         = "../lambda_function/webhook_receiver.zip"
  function_name    = "${var.prefix}-tfe-webhook-receiver"
  role             = aws_iam_role.iam_for_lambda.arn
  source_code_hash = data.archive_file.webhook_receiver_zip.output_base64sha256
  handler          = "main.lambda_handler"
  runtime          = "python3.7"
  timeout          = 30

  environment {
    variables = {
      DYNAMO_TABLE_NAME = aws_dynamodb_table.base_dynamodb_table.id
    }
  }

  depends_on = [data.archive_file.webhook_receiver_zip]
}
