resource "aws_dynamodb_table" "base_dynamodb_table" {
  name           = "${var.prefix}-tfe-webhooks"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "id"
  range_key      = "timestamp"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "N"
  }

}
