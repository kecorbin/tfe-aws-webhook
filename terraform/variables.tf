variable "region" {
  description = "The region where the resources are created."
  default     = "us-east-1"
}

variable "prefix" {
  description = "Prefix for the resources that get created"
}
