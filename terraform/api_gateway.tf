resource "aws_api_gateway_rest_api" "wh_receiver" {
  name        = "${var.prefix}-TFE-WH-RCVR"
  description = "Webhook Receiver for Terraform Enterprise"
}

resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.wh_receiver.id
  parent_id   = aws_api_gateway_rest_api.wh_receiver.root_resource_id
  path_part   = "wh"
}

resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = aws_api_gateway_rest_api.wh_receiver.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = aws_api_gateway_rest_api.wh_receiver.id
  resource_id = aws_api_gateway_method.proxy.resource_id
  http_method = aws_api_gateway_method.proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.receiver_lambda.invoke_arn
}


resource "aws_api_gateway_deployment" "prod_api" {
  depends_on = [
    aws_api_gateway_integration.lambda,
    aws_api_gateway_resource.proxy
  ]

  rest_api_id = aws_api_gateway_rest_api.wh_receiver.id
  stage_name  = "Production"
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.receiver_lambda.arn
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_deployment.prod_api.execution_arn}/*/*"
}
