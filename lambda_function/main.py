import os
import time
import uuid
import json
import boto3

dynamo_table_name = os.environ["DYNAMO_TABLE_NAME"]

#Configure DynamoDB
dyn = boto3.resource('dynamodb')
table = dyn.Table(dynamo_table_name)

def lambda_handler(event, context):
    print(event)
    if event['httpMethod'] == "POST":
        return post(event)
    else:
        return get(event)

def get(event):
    response = [json.loads(i['payload']) for i in table.scan()['Items']]

    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(response)
        }

def post(event):
    payload = json.loads(event['body'])
    if payload and 'run_status' in payload['notifications'][0]:
        table.put_item(
            Item={
                'id' : str(uuid.uuid4()),
                'timestamp': int(time.time()),
                'payload' : json.dumps(payload)
            }
        )

    return {
        "statusCode": 200,
        "body": json.dumps('feelin alright')
    }
