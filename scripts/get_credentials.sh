#!/bin/sh

# env vars seeded in CI pipeline
echo "Requesting/Generating required credentials from HashiCorp Vault"

# Login to vault using approle
export VAULT_TOKEN=$(curl -s \
    --request POST \
    -H "X-Vault-Namespace: ${VAULT_NAMESPACE}" \
    --data "{\"role_id\":\"${VAULT_APPROLE_ID}\" ,\"secret_id\":\"${VAULT_APPROLE_SECRET_ID}\"}" \
    $VAULT_ADDR/v1/auth/approle/login | jq -r .auth.client_token)

# display info about our token
vault token lookup

# get some AWS creds for awscli
echo "Requesting AWS Credentials from Vault"
CREDS=$(vault read aws/creds/webhook-s3-creds -format=json)
echo "IAM credentials are eventually consistent with respect to other Amazon services. Waiting 20s... "
sleep 20

# create awscli config
export AWS_DEFAULT_REGION=us-east-1
mkdir ~/.aws
cat << EOF > ~/.aws/credentials
[default]
aws_access_key_id = $(echo $CREDS | jq -r .data.access_key)
aws_secret_access_key = $(echo $CREDS | jq -r .data.secret_key)
EOF
aws configure list

# get TFE token from kv secrets engine
echo "Getting TFE Credentials from Vault"
export TFE_TOKEN=$(vault kv get -field=token kv/tfe)
export TFE_HOST=$(vault kv get -field=host kv/tfe)

# write terraform config
cat << EOF > ~/.terraformrc
credentials "$TFE_HOST" {
  token = "$TFE_TOKEN"
}
EOF

# tf-helper credentials
mkdir ~/.tfh
cat << EOF > ~/.tfh/tfhrc
TFH_org=$TFE_ORG
TFH_token=$TFE_TOKEN
EOF


