#!/bin/sh
# This script is ran at the beginning of gitlab CI pipelines and will create a new
# workspace for the working branch if one does not already exist, provision any required 
# variables and update the remote backend configuration for terraform to use

export WORKSPACE_NAME="tfe-aws-webhook-${CI_COMMIT_BRANCH}"
export TFE_ORG="kcorbin"
export TFH_org=$TFE_ORG
export PREFIX=${CI_COMMIT_BRANCH}

# Login to vault using approle
export VAULT_TOKEN=$(curl -s \
    --request POST \
    -H "X-Vault-Namespace: ${VAULT_NAMESPACE}" \
    --data "{\"role_id\":\"${VAULT_APPROLE_ID}\" ,\"secret_id\":\"${VAULT_APPROLE_SECRET_ID}\"}" \
    $VAULT_ADDR/v1/auth/approle/login | jq -r .auth.client_token)


echo "Checking if workspace $WORKSPACE_NAME already exists"
tfh workspace show $WORKSPACE_NAME
if [ $? -eq 0 ]
then
  echo "Workspace exists"
else
  echo "Creating workspace $WORKSPACE_NAME"
  tfh workspace new $WORKSPACE_NAME
fi

# update terraform working directory
tfh workspace update $WORKSPACE_NAME -working-dir terraform

# generate AWS creds for terraform workspace
echo "Retrieving AWS Credentials for Workspace from Vault"
WORKSPACE_CREDS=$(vault read aws/creds/terraform -format=json)
AWS_ACCESS_KEY_ID=$(echo ${WORKSPACE_CREDS} | jq -r .data.access_key)
AWS_SECRET_ACCESS_KEY=$(echo ${WORKSPACE_CREDS} | jq -r .data.secret_key)

echo "Configuring Variables"
echo "Configuring Settings/Variables"
tfh workspace update $WORKSPACE_NAME -working-dir terraform -auto-apply
tfh pushvars -org $TFE_ORG -name $WORKSPACE_NAME -overwrite-all -dry-run false \
    -var "prefix=$PREFIX" \
    -env-var "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID" \
    -senv-var "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY" \

# dont allow protected branches to be destroyed
if [ "${CI_COMMIT_REF_PROTECTED}" = "true" ]
then
  echo "Running on Protected Branch "
  tfh pushvars -org $TFE_ORG -name $WORKSPACE_NAME -delete-env "CONFIRM_DESTROY"
else
  echo "Protected branch is set to ${CI_COMMIT_REF_PROTECTED}, allow workspace to be destroyed"
  tfh pushvars -org $TFE_ORG -name $WORKSPACE_NAME -env-var "CONFIRM_DESTROY=1"   

fi

cat << EOF > terraform/remote_backend.tf
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "$TFE_ORG"
    workspaces {
      name = "$WORKSPACE_NAME"
    }
  }
}
EOF

echo "Added remote backend configuration to terrform"
cat terraform/remote_backend.tf